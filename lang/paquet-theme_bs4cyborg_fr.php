<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4cyborg
// Langue: fr
// Date: 26-03-2020 16:49:58
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4cyborg_description' => 'Jet black and electric blue',
	'theme_bs4cyborg_slogan' => 'Jet black and electric blue',
);
?>